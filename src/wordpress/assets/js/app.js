import { Application, ControllerServiceProvider } from '@cherrypulp/trunk';

const app = new Application();

// Register Service Providers
app.register( new ControllerServiceProvider( app ) );

document.addEventListener('DOMContentLoaded', () => app.boot() );
