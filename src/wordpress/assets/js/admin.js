// required external components
import $ from 'jquery';

// required internal components
import {getYoutubeId, slugify} from './lib/helpers';


//$(document).ready(function () {});

// @see https://www.advancedcustomfields.com/resources/adding-custom-javascript-fields/
function initialize($el) {
    $el.find('.input-slugify').each(function () {
        const $parent = $(this);
        const $input = $parent.find('input[type="text"]');

        $input
            .on('keyup', function () {
                const $this = $(this);
                const timeout = $this.data('timeout');

                if (timeout) {
                    clearTimeout(timeout);
                }

                $this.data('timeout', setTimeout(function () {
                    const value = $.trim($this.val());

                    // @note Slugify only if it is not an url
                    if (!/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/.test(value)) {
                        $this.val(slugify(value));
                    }
                }, 600));
            });
    });

    $el.find('.input-youtube').each(function () {
        const $parent = $(this);
        const $input = $parent.find('input[type="text"]');

        // Build video container
        if (!$parent.find('.acf-field-input-youtube').length) {
            const template = `<div class="acf-field acf-field-input-youtube">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="" frameborder="0" allowfullscreen></iframe>
                    </div>                
                </div>`;
            $parent.find('.acf-input').append(template);
        }

        const $iframe = $parent.find('.embed-responsive-item');

        $input
            .on('input paste keyup change', function () {
                const $this = $(this);
                const timeout = $this.data('timeout');

                if (timeout) {
                    clearTimeout(timeout);
                }

                $this.data('timeout', setTimeout(function () {
                    const youtubeId = getYoutubeId($.trim($input.val()));

                    if (!youtubeId) {
                        // if not a youtube video -> empty the iframe
                        $iframe.attr('src', '');
                    } else if (youtubeId !== getYoutubeId($iframe.attr('src'))) {
                        // if a youtube video and different that the current one
                        $iframe.attr('src', `//www.youtube.com/embed/${youtubeId}`);
                    }
                }, 600));
            })
            .trigger('input');
    });
}

if (window.acf) {
    const acf = window.acf;

    acf.add_action('load', initialize);
    acf.add_action('append', initialize);
    // acf.add_action('ready', initialize);
}

