(function (tinymce) {
    'use strict';

    // @see https://www.gavick.com/blog/wordpress-tinymce-custom-buttons
    // @see http://stackoverflow.com/a/30098931/1420009
    tinymce.PluginManager.add('stem_shortcodes', function (editor, url) {
        /*editor.addButton('stem_br', {
         title: 'Insert bg tag',
         icon: 'wp_code',
         onclick: function () {
         editor.insertContent('<br>');
         }
         });*/

        editor.addButton('stem_shortcodes', {
            title: 'Insert custom shortcode',
            icon:  'cherrypulp-icon',
            type:  'menubutton',
            menu:  [
                {
                    text: 'Insert br tag',
                    onclick: function () {
                        editor.insertContent('<br>');
                    }
                },
                {
                    text:    'Insert button',
                    onclick: function () {
                        editor.windowManager.open({
                            title:    'Insert button',
                            body:     [
                                {
                                    type:  'textbox',
                                    name:  'text',
                                    label: 'Button text'
                                },
                                {
                                    type:  'textbox',
                                    name:  'url',
                                    label: 'Button link'
                                },
                                {
                                    type:   'listbox',
                                    name:   'target',
                                    label:  'Button target',
                                    values: [
                                        {
                                            text:  'default',
                                            value: '_self'
                                        },
                                        {
                                            text:  'new window',
                                            value: '_blank'
                                        },
                                    ]
                                },
                            ],
                            onsubmit: function (e) {
                                var template = '[button url="' + e.data.url + '" style="' + e.data.style + '" size="' + e.data.size + '" target="' + e.data.target + '"]' + e.data.text + '[/button]';
                                editor.insertContent(template);
                            }
                        });
                    }
                },
                {
                    text:    'Insert icon',
                    onclick: function () {
                        editor.windowManager.open({
                            title:    'Insert icon',
                            body:     [
                                {
                                    type:  'textbox',
                                    name:  'type',
                                    label: 'Icon type (see http://fortawesome.github.io/Font-Awesome/icons)'
                                },
                                {
                                    type:  'textbox',
                                    name:  'color',
                                    label: 'Icon color'
                                },
                            ],
                            onsubmit: function (e) {
                                var template = '[icon type="' + e.data.type + '" color="' + e.data.color + '"]';
                                editor.insertContent(template);
                            }
                        });
                    }
                },
                {
                    text:    'Insert panel',
                    onclick: function () {
                        editor.windowManager.open({
                            title:    'Insert panel',
                            body:     [
                                {
                                    type:  'textbox',
                                    name:  'title',
                                    label: 'Panel title'
                                },
                                {
                                    type:  'textbox',
                                    name:  'text',
                                    label: 'Panel content'
                                },
                                {
                                    type:   'listbox',
                                    name:   'style',
                                    label:  'Panel style',
                                    values: [
                                        {
                                            text:  'default',
                                            value: 'default'
                                        },
                                        {
                                            text:  'primary',
                                            value: 'primary'
                                        },
                                        {
                                            text:  'info',
                                            value: 'info'
                                        },
                                        {
                                            text:  'success',
                                            value: 'success'
                                        },
                                        {
                                            text:  'warning',
                                            value: 'warning'
                                        },
                                        {
                                            text:  'danger',
                                            value: 'danger'
                                        },
                                    ]
                                },
                            ],
                            onsubmit: function (e) {
                                var template = '[panel style="' + e.data.style + '" title="' + e.data.title + '"]' + e.data.text + '[/panel]';
                                editor.insertContent(template);
                            }
                        });
                    }
                },
                {
                    text:    'Insert columns',
                    onclick: function () {
                        editor.windowManager.open({
                            title:    'Insert columns',
                            body:     [
                                {
                                    type: 'container',
                                    name: 'container',
                                    label: 'Example',
                                    html: '<code>[column md="6" md-offset="2"][/column]</code>',
                                },
                                {
                                    type:   'listbox',
                                    name:   'md',
                                    label:  'MD size',
                                    values: [
                                        {
                                            text:  'Undefined',
                                            value: 'undefined'
                                        },
                                        {
                                            text:  '1',
                                            value: '1'
                                        },
                                        {
                                            text:  '2',
                                            value: '2'
                                        },
                                        {
                                            text:  '3',
                                            value: '3'
                                        },
                                        {
                                            text:  '4',
                                            value: '4'
                                        },
                                        {
                                            text:  '5',
                                            value: '5'
                                        },
                                        {
                                            text:  '6',
                                            value: '6'
                                        },
                                        {
                                            text:  '7',
                                            value: '7'
                                        },
                                        {
                                            text:  '8',
                                            value: '8'
                                        },
                                        {
                                            text:  '9',
                                            value: '9'
                                        },
                                        {
                                            text:  '10',
                                            value: '10'
                                        },
                                        {
                                            text:  '11',
                                            value: '11'
                                        },
                                        {
                                            text:  '12',
                                            value: '12'
                                        },
                                    ]
                                },
                                {
                                    type:   'listbox',
                                    name:   'md-offset',
                                    label:  'MD offset size',
                                    values: [
                                        {
                                            text:  'Undefined',
                                            value: 'undefined'
                                        },
                                        {
                                            text:  '1',
                                            value: '1'
                                        },
                                        {
                                            text:  '2',
                                            value: '2'
                                        },
                                        {
                                            text:  '3',
                                            value: '3'
                                        },
                                        {
                                            text:  '4',
                                            value: '4'
                                        },
                                        {
                                            text:  '5',
                                            value: '5'
                                        },
                                        {
                                            text:  '6',
                                            value: '6'
                                        },
                                        {
                                            text:  '7',
                                            value: '7'
                                        },
                                        {
                                            text:  '8',
                                            value: '8'
                                        },
                                        {
                                            text:  '9',
                                            value: '9'
                                        },
                                        {
                                            text:  '10',
                                            value: '10'
                                        },
                                        {
                                            text:  '11',
                                            value: '11'
                                        },
                                        {
                                            text:  '12',
                                            value: '12'
                                        },
                                    ]
                                },
                            ],
                            onsubmit: function (e) {
                                var attributes = [];

                                console.log('---))))', e.data);

                                Object.keys(e.data).map(function (key) {
                                    var value = e.data[key];
                                    if (key !== 'container' && value !== 'undefined') {
                                        attributes.push(key + '="' + value + '"');
                                    }
                                });

                                var template = '[column ' + attributes.join(' ') + '] __content__ [/column]';

                                editor.insertContent('[row]' + template + '[/row]');
                            }
                        });
                    }
                },
                {
                    text:    'Insert divider',
                    onclick: function () {
                        editor.windowManager.open({
                            title:    'Insert divider',
                            body:     [
                                {
                                    type:   'listbox',
                                    name:   'size',
                                    label:  'Divider size',
                                    values: [
                                        {
                                            text:  '1',
                                            value: '1'
                                        },
                                        {
                                            text:  '2',
                                            value: '2'
                                        },
                                        {
                                            text:  '3',
                                            value: '3'
                                        },
                                        {
                                            text:  '4',
                                            value: '4'
                                        },
                                        {
                                            text:  '5',
                                            value: '5'
                                        },
                                    ]
                                },
                                {
                                    type:   'checkbox',
                                    name:   'hidden',
                                    label:  'Hidden',
                                    checked: true
                                },
                            ],
                            onsubmit: function (e) {
                                var template = '[divider size="' + e.data.size + '" hidden="' + e.data.hidden + '"][/divider]';

                                editor.insertContent(template);
                            }
                        });
                    }
                },
            ]
        });
    });
})(window.tinymce);
