import { Controller } from '@cherrypulp/trunk';

export default class DefaultController extends Controller {
    /**
     * Condition to execute controller.
     *
     * @return {boolean}
     */
    isActive() {
        return true;
    }

    /**
     * Mounted.
     */
    mounted() {

    }
}
