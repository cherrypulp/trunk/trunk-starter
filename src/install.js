#!/usr/bin/env node

const readline = require('readline');
const fs = require('fs');
const path = require('path');
const user_path = process.env.INIT_CWD;

/** Trunk is installed on a Laravel project */
if ( fs.existsSync( user_path + '/artisan' ) ) {
    laravel();

    process.exit( 0 );
}

/** Trunk is installed inside a WordPress Theme */
if ( fs.existsSync( user_path + '/style.css' ) ) {
    wordpress();

    process.exit( 0 );
}

/** We cannot guess which project type is it so we ask the user : */

const rl = readline.createInterface( {
    input: process.stdin,
    output: process.stdout
} );

rl.question( 'Do you install trunk using Laravel or WordPress ? ', function ( name ) {
    switch ( name.toLowerCase() ) {
        case 'laravel' :
            laravel();
            break;

        case 'wordpress' :
            wordpress();
            break;

        default:
            break;
    }

    rl.close();
} );

rl.on( 'close', function () {
    process.exit( 0 );
} );

function laravel() {
    copy('laravel');
}

function wordpress() {
    copy('wordpress');
}

function copy(target) {
    copyFolderSync('./' + target, user_path);

    console.log( 'Trunk files have been copied in your project' );
}

function copyFolderSync(from, to) {
    if ( !fs.existsSync( to ) ) {
        fs.mkdirSync( to );
    }

    fs.readdirSync( from ).forEach( element => {
        if ( fs.lstatSync( path.join( from, element ) ).isFile() ) {
            fs.copyFileSync( path.join( from, element ), path.join( to, element ) );

            if ( element === '.gitkeep' ) {
                console.log( '=> ' + path.join( to, '/') );
            } else {
                console.log( '=> ' + path.join( to, element ) );
            }

        } else {
            copyFolderSync( path.join( from, element ), path.join( to, element ) );
        }
    });
}