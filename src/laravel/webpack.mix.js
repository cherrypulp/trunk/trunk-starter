const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js( 'resources/js/app.js', 'public/dist/js' )
    .sass( 'resources/sass/app.scss', 'public/dist/css' );

mix.copyDirectory( 'resources/img', 'public/dist/img' );
mix.copyDirectory( 'resources/fonts', 'public/dist/fonts' );

mix.options( {
    processCssUrls: false
} );

mix.webpackConfig( {
    resolve: {
        alias: {
            '@': path.resolve( __dirname, 'assets' ),
        },
    },
} );
