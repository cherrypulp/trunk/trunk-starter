# Trunk Starter

> Install and initialise a project using Trunk and Laravel Mix

## Installation

Make sure all dependencies have been installed before moving on:

- [Node.js](http://nodejs.org/) >= 8.0.0

```console
npm install --saveDev @cherrypulp/trunk-starter
```

## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Anthony Pauwels** - [anthonypauwels](https://gitlab.com/anthonypauwels) / [website](https://anthonypauwels.be/)

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/cherrypulp/trunk/trunk-starter/blob/master/LICENSE) file for details.

## See also

- [Trunk](https://www.npmjs.com/package/@cherrypulp/trunk)

## TODO

- [ ] unit tests
- [ ] e2e tests
